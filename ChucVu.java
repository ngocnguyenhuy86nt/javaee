/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlynhansu;

/**
 *
 * @author Admin
 */
public class ChucVu {
    private String maCV;
    private String tenCV;  
    
    public ChucVu(String _maCV, String _tenCV)
    {
        this.maCV = _maCV;
        this.tenCV = _tenCV;  
    }
    
    public String getMaCV()
    {
        return this.maCV;
    }
    
    public String getTenCV()
    {
        return this.tenCV;
    }
}
