/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlynhansu;

/**
 *
 * @author Admin
 */
public class PhongBan {
    private String maPB;
    private String tenPB;
    private String diaChi;
    private String sdtPB;
    
    PhongBan(String _maPB, String _tenPB, String _diaChi, String _sdtPB)
    {
        this.maPB = _maPB;
        this.tenPB = _tenPB;
        this.diaChi = _diaChi;
        this.sdtPB = _sdtPB;
    }
    
    public String getMaPB()
    {
        return this.maPB;
    }
    
    public String getTenPB()
    {
        return this.tenPB;
    }
        
    public String getDiaChi()
    {
        return this.diaChi;
    }
    
    public String getSDTPB()
    {
        return this.sdtPB;
    }
}
